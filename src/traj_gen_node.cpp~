/****************************************************************************

Conversion from vicon pose/quaternion to mavros pose/quaternion

Nodes:
subscribed pose and quat from Vicon (geometry_msgs::TransformStamped)
published  pose and quat to MAVROS (geometry_msgs::PoseStamped)
both in 

****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include "traj_gen_node/wp_com.h"
#include <pthread.h>
#include <math.h>
#define PI 3.14159265
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))


// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;

// Global Vars

// Local Pose
geometry_msgs::PoseStamped loc_pos;

// Index Count
int wp_count = 0;

// Error Thresholds
double z_thresh = 0.15; // m
double cart_thresh = 0.3; // m
double yaw_thresh = 5*PI/180; // radians

void posCallback(const geometry_msgs::PoseStamped& msg)
{
  loc_pos = msg;
  pthread_cond_signal(&newPose);
}

int main(int argc, char **argv)
{
  // Ros Init
  ros::init(argc, argv, "traj_gen_node");
  // Ros Handle
  ros::NodeHandle n;
  
  // WP Tables:
  /*
  double x_wp_arr[4] = {5.0,-5.0,-5.0,5.0};// m
  double y_wp_arr[4] = {5.0,5.0,-5.0,-5.0};// m
  double z_wp_arr[4] = {2.0,2.0,2.0,2.0};// m
  double g_wp_arr[4] = {0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180};// rad 
  */
  
  double a1 = 5.0;
  double a2 = 3.5355;
  double x_wp_arr[8] = {a1,a2,0.0,-a2,-a1,-a2,0.0,a2};// m
  double y_wp_arr[8] = {0.0,a2,a1,a2,0.0,-a2,-a1,-a2};// m
  double z_wp_arr[8] = {2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0};// m
  double g_wp_arr[8] = {0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180,0.0*PI/180};// rad 
  
  
  //
  int wp_mod = NELEMS(x_wp_arr);
  int wp_ind = 0; 
  int close_count = 0;
  double x_wp = 0;//x_wp_arr[wp_ind];
  double y_wp = 0;//y_wp_arr[wp_ind];
  double z_wp = 2;//z_wp_arr[wp_ind];
  double g_wp = 0;//g_wp_arr[wp_ind];
  
  // Errors
  double z_err, x_err, y_err, cart_err, yaw_err;
  
  //======= ROS Publishers ================
  // MAKE PUBLISHER
  ros::Publisher wp_pub = n.advertise<traj_gen_node::wp_com>("com_val",1000);  
  traj_gen_node::wp_com wp_out;
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);
  // Setup Subscribers:
  ros::Subscriber sub_pos = n.subscribe("pose", 1000, posCallback);
  // Start the Spinner
  spinner.start();
  
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  
  while (ros::ok())
  {
    //Only publish when there's a new pose to share
    
    //Return false if we timed out in the cond_wait
    struct timespec timeout;
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_sec +=1;
    int ret;
    ret = pthread_cond_timedwait(&newPose, &topicBell, &timeout);
    if (ret == ETIMEDOUT)
    {
	    ROS_INFO("No mavros local poses received!");
	    continue;
    }
    
    // Error Calcs
	z_err = (loc_pos.pose.position.z-z_wp);
        if(z_err<0){
	        z_err = z_err*-1;
        }
	x_err = (loc_pos.pose.position.x-x_wp);
    y_err = (loc_pos.pose.position.y-y_wp);
   cart_err = sqrt(pow(x_err,2.0)+pow(y_err,2.0));
   // Yaw Err is TBD
   // yaw_err = etc....
   ROS_INFO("Zerr %f CartErr %f",z_err,cart_err); 
   if((z_err<z_thresh) && (cart_err<cart_thresh)){
       ++close_count;
           if(close_count>=0){
               ++wp_count;
   		       wp_ind = (wp_count % wp_mod); 
   		       x_wp = x_wp_arr[wp_ind];
  		       y_wp = y_wp_arr[wp_ind];
  		       z_wp = z_wp_arr[wp_ind];
  		       g_wp = g_wp_arr[wp_ind];
		       close_count = 0;
           }
   	}	
   	
   	wp_out.x = x_wp;
   	wp_out.y = y_wp;
   	wp_out.z = z_wp;
   	wp_out.yaw = g_wp;
    wp_pub.publish(wp_out);
  }
  return 0;
}
